﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour {

    public string name;

    private Vector3 pos;



    private void Update()
    {
        
          
    }
    private void OnMouseEnter()
    {
      //  Debug.Log("Enter");
     
        pos =GetComponent<Transform>().position;
    }
    private void OnMouseOver()
    {
        // Debug.Log("Over");
        if (GetComponent<SpriteRenderer>().sprite == null ? false : true)
            GetComponent<Transform>().position = pos + Vector3.up * .5f;
    }
    private void OnMouseExit()
    {
       // Debug.Log("exit");
        GetComponent<Transform>().position=pos;
    }
    private void OnMouseDown()
    {
        if (GameObject.FindObjectOfType<GameManager>().sira == false)
            return;
        if(GetComponent<SpriteRenderer>().sprite!=null)
        GameObject.FindObjectOfType<GameManager>().PlayerPlay(name);
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<Transform>().position = pos + Vector3.up * 15f;
        GetComponent<SpriteRenderer>().sprite = null;

    }

    
}
