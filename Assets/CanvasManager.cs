﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class CanvasManager : MonoBehaviour {

    public Image panel;
    public Image kazananPaneli;
    public List<Image> kartlar = new List<Image>(4);

    public GameObject btn;

    private GameManager gm;

    private void OnEnable()
    {
      

    }
    public void FazlaKartBtn()
    {
        panel.gameObject.SetActive(false);
        btn.gameObject.SetActive(false);
    }

    internal void Goster()
    {
        gm = GameObject.FindObjectOfType<GameManager>();
        for (int i = 0; i < gm.yerdekiFazlaKartlar.Count; i++)
        {
            kartlar[i].sprite = Resources.Load<Sprite>("Kartlar/" + gm.yerdekiFazlaKartlar[i]);
        }
        panel.gameObject.SetActive(true);
        btn.SetActive(true);
    }
    public void Kazandin()
    {
        kazananPaneli.gameObject.SetActive(true);
        kazananPaneli.GetComponentInChildren<Text>().text = "Kazanan" + (gm.p1.genelPuan > gm.p2.genelPuan ? gm.p1.isim : gm.p2.isim);
    }
    public void Kazanan()
    {
        SceneManager.LoadScene(0);
    }
}
