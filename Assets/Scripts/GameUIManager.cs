﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUIManager : MonoBehaviour
{
    
    public TextMesh textDeste;
    public TextMesh textPlayerKaszanilan;
    public TextMesh textPcKazanilan;
    public TextMesh textPlayerSkor;
    public TextMesh textPcSkor;

    public Transform[] oyuncu;
    public Transform[] bilgisayar;
    public List<SpriteRenderer> enableOnStart;
    public List<Sprite> spriteDeste;


    public SpriteRenderer center;
    public SpriteRenderer Pc;
    public SpriteRenderer Pl;

    public GameManager gm;
    public Animator animator;
    public GameObject startButon;
    public CanvasManager fazlaKartPaneli;


    private AudioSource audioManager;
    public AudioClip[] sesler;

    private void OnEnable()
    {
        gm = GameObject.FindObjectOfType<GameManager>();
        animator = GetComponent<Animator>();
        audioManager = GetComponent<AudioSource>();
        if (gm == null)
        {
            Debug.Log("GameManager bulunamadı!!!");
            Destroy(this);
        }
        gm.Pisti += PistiOldU;//pişdi için event ekleme
        gm.YerdenKartAlindi += YerdenKartALindi;//normal kazanma için event
        gm.HamleYapma += HamleYapma;//Yere kart atma  hareketi icin event
        gm.YerdekiFazlaKartlarAlindi += YerdekiFazlaKartlarAlindi;
    }

    public void DesteDagit()
    {
        startButon.SetActive(false);
        textDeste.gameObject.SetActive(true);

        for (int i = 0; i < enableOnStart.Count; i++)
        {
            enableOnStart[i].enabled = true;

        }

        foreach (Transform item in oyuncu)
        {
            item.GetComponent<BoxCollider>().enabled = true;
        }

        for (int i = 0; i < gm.p1.kart.Count; i++)
        {
            oyuncu[i].GetComponent<Card>().name = gm.p1.kart[i];
            oyuncu[i].GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("kartlar/" + gm.p1.kart[i]);
        }

        for (int i = 0; i < gm.p2.kart.Count; i++)
        {
            bilgisayar[i].GetComponent<Card>().name = gm.p2.kart[i];
            bilgisayar[i].GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("kartlar/K");
            bilgisayar[i].GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("kartlar/K");

        }
        animator.SetTrigger("Dagit");
        textDeste.text = gm.deste.Count+"";
    }

    public void YereKartAt(string name, bool bilgisayarMi)
    {

        center.sprite = Resources.Load<Sprite>("kartlar/" + name);

        if (bilgisayarMi)
            for (int i = 0; i < bilgisayar.Length; i++)
            {
                if (bilgisayar[i].GetComponent<Card>().name == name)
                    bilgisayar[i].GetComponent<SpriteRenderer>().sprite = null;
            }
    }

    public void SkorYazdir(int skor,bool bilgisayarMi)
    {
        if (!bilgisayarMi)
        {
            textPlayerSkor.gameObject.SetActive(true);
            textPlayerSkor.text="Oyuncu("+skor+")";
        }
        else
        {
            textPcSkor.gameObject.SetActive(true);
            textPcSkor.text = "Bilgisayar(" + skor + ")";
        }

    }
    public void KazanilanKart(string kart, bool bilgisyarMi)
    {
        center.sprite = null;
        if (!bilgisyarMi)
        {
           Pl.sprite = Resources.Load<Sprite>("kartlar/"+kart);
            textPlayerKaszanilan.gameObject.SetActive(true);
            textPlayerKaszanilan.text = gm.p1.kazanilanKartlar.Count+"";
        }
        else
        {
            Pc.sprite = Resources.Load<Sprite>("kartlar/" + kart);
            textPcKazanilan.gameObject.SetActive(true);
            textPcKazanilan.text = gm.p2.kazanilanKartlar.Count + "";
        }

    }

    public void OyunKur()
    {
        Pl.sprite = null;
        Pc.sprite = null;
        textPcKazanilan.gameObject.SetActive(false);
        textPlayerKaszanilan.gameObject.SetActive(false);
        center.sprite = Resources.Load<Sprite>("kartlar/" +gm.yerdekiFazlaKartlar[gm.yerdekiFazlaKartlar.Count - 1]);
        gm.yerdekiKartlar.Add(gm.yerdekiFazlaKartlar[gm.yerdekiFazlaKartlar.Count - 1]);
        gm.yerdekiFazlaKartlar.RemoveAt(gm.yerdekiFazlaKartlar.Count - 1);
    }

    void PistiOldU(object sender,EventArgs e)
    {
        Debug.Log("Pisti");
        audioManager.clip = sesler[2];
        audioManager.Play();
    }

    void YerdenKartALindi(object sender,EventArgs e)
    {
        Debug.Log("Yerden kart alındı");
        audioManager.clip = sesler[1];
        audioManager.Play();

    }
    void HamleYapma(object sender,EventArgs e)
    {
        audioManager.clip = sesler[0];
        audioManager.Play();
    }

    void YerdekiFazlaKartlarAlindi (object sender, EventArgs e)
    {
        if(gm.sira)
        fazlaKartPaneli.Goster();

    }
}
