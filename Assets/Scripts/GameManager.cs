﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    /*Deste
     * 10 adet Diamonds(Karo) kare
     * 10 adet Hearts(Kupa) kalp
     * 10 adet Spades(Maça) teskalp
     * 10 det Clubs(Sinek) yonca
     *  4'er J,Q,K
     *  Toplamda 52 kart 
     * */


    public GameUIManager gmUIManager;

    public Player p1;//Oyuncu

    public Player p2;//Bilgisayar
    public bool sira = true;//true oynucu false bilgisayar.


    public List<string> deste = new List<string>(52);
    public List<string> yerdekiKartlar = new List<string>();
    public List<string> yerdekiFazlaKartlar = new List<string>(4);


    private Player sonKazanan;
    private int tur = 0;
    private int el = 0;

    #region TestAlani
    public UnityEngine.UI.InputField input;
    #endregion
    private void Awake()
    {
        p1 = new Player("Oyuncu");
        p2 = new Player("Bilgisayar");


    }



    public void OynuBaSlat()
    {
        //UI da kartları dağıt
        //ve oyuncunun oynamasını bekle
        OynuKur();

        //for (int i = 0; i < p1.kart.Count; i++)
        //{
        //    Debug.Log(p1.isim + " " + p1.kart[i]);
        //}
        //for (int i = 0; i < p2.kart.Count; i++)
        //{
        //    Debug.Log(p2.isim + " " + p2.kart[i]);
        //}
    }


    private float zamanDegiskeni;
    private void Update()
    {
        if (!sira)
        {
            if (Time.time > zamanDegiskeni)
            {
                if (yerdekiKartlar.Count <= 0)
                    YeraKartAt(p2, p2.Oyna("", 0));
                else
                    YeraKartAt(p2, p2.Oyna(yerdekiKartlar[yerdekiKartlar.Count - 1], yerdekiKartlar.Count));
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            Logla();
        }
    }

    public void Logla()
    {
        Debug.Log("Tur:" + tur + " El:" + el);
        #region Pile_log
        string yerdeki_Kartlari = "";
        if (yerdekiKartlar.Count > 0)
        {
            foreach (string item in yerdekiKartlar)
            {
                yerdeki_Kartlari += item + "|";
            }
            yerdeki_Kartlari = yerdeki_Kartlari.LastIndexOf('|') == yerdeki_Kartlari.Length - 1 ? yerdeki_Kartlari.Substring(0, yerdeki_Kartlari.Length - 1) : yerdeki_Kartlari;
        }
        Debug.Log("Yerdeki Kartlar=" + yerdeki_Kartlari);
        #endregion
        #region Player_Log
        string player_Kartlari = "";
        if (p1.kart.Count > 0)
        {
            foreach (string item in p1.kart)
            {
                player_Kartlari += item + "|";
            }
            player_Kartlari = player_Kartlari.LastIndexOf('|') == player_Kartlari.Length - 1 ? player_Kartlari.Substring(0, player_Kartlari.Length - 1) : player_Kartlari;
        }
        Debug.Log("Oyuncunun_Eli=" + player_Kartlari);
        #endregion

        #region Pc_log
        string pc_Kartlari = "";
        if (p2.kart.Count > 0)
        {
            foreach (string item in p2.kart)
            {
                pc_Kartlari += item + "|";
            }
            pc_Kartlari = pc_Kartlari.LastIndexOf('|') == pc_Kartlari.Length - 1 ? pc_Kartlari.Substring(0, pc_Kartlari.Length - 1) : pc_Kartlari;
        }
        Debug.Log("Bilgisayarın_Eli=" + pc_Kartlari);
        #endregion
    }

    public void PlayerPlay(string card)
    {
        if (sira)
            YeraKartAt(p1, card);
        else
            Debug.Log("Sıranı bekle");

    }

    public void YeraKartAt(Player oyuncu, string kart)
    {
        if (sira == false && oyuncu == p1)
            return;


        Hamle(EventArgs.Empty);

        if (oyuncu == p2)
            gmUIManager.YereKartAt(kart, true);
        if (oyuncu == p1)
            zamanDegiskeni = Time.time + UnityEngine.Random.Range(.5f, 1.5f);
        gmUIManager.YereKartAt(kart, false);
        if (oyuncu.kart.Contains(kart))
            oyuncu.kart.Remove(kart);
        else
        {
            Debug.LogError("Olmayan bir kartı yere atamazsınız.:D");
            return;
        }

        yerdekiKartlar.Add(kart);
        int sonKart = yerdekiKartlar.Count - 1;
        if (yerdekiKartlar.Count >= 2)
            if (yerdekiKartlar[sonKart].Split('_')[1] == yerdekiKartlar[sonKart - 1].Split('_')[1] || yerdekiKartlar[sonKart].Split('_')[1] == "11")
            {
                sonKazanan = oyuncu;
                YerdenKartAl(EventArgs.Empty);

                // Debug.Log("kartları aldın");
                if (yerdekiFazlaKartlar.Count > 0)
                {
                    Debug.Log(oyuncu.isim + " Yerdeki fazla kartlarıda aldı!!!");//ilk dağıtımdaki fazla kartlarıda alma durumu.
                    for (int i = 0; i < yerdekiFazlaKartlar.Count; i++)
                    {
                        oyuncu.kazanilanKartlar.Add(yerdekiFazlaKartlar[i]);
                    }
                    YerdekiFazlaKartlaralindi(EventArgs.Empty);
                    yerdekiFazlaKartlar.Clear();
                }
                if (yerdekiKartlar.Count == 2 && yerdekiKartlar[sonKart].Split('_')[1] == yerdekiKartlar[sonKart - 1].Split('_')[1])
                {
                    // Debug.Log(oyuncu.isim + " pişti");
                    PistiOldu(EventArgs.Empty);
                    oyuncu.pistiler.Add(yerdekiKartlar[0]);

                }
                for (int i = 0; i < yerdekiKartlar.Count; i++)
                {
                    oyuncu.kazanilanKartlar.Add(yerdekiKartlar[i]);
                }


                yerdekiKartlar.Clear();


                if (oyuncu == p1)
                    gmUIManager.KazanilanKart(kart, false);
                else if (oyuncu == p2)
                    gmUIManager.KazanilanKart(kart, true);

               // PuanHesapla();

            }
        sira = !sira;


        //for (int i = 0; i < p1.kart.Count; i++)
        //{
        //    Debug.Log(p1.isim + " " + p1.kart[i]);
        //}
        //for (int i = 0; i < p2.kart.Count; i++)
        //{
        //    Debug.Log(p2.isim + " " + p2.kart[i]);
        //}

        if (p1.kart.Count == 0 && p2.kart.Count == 0)//Eğer iki oyuncudada kart kalmadıysa kart çek
        {
            DestedenKartCek();
        }
    }

    private void DestedenKartCek()
    {

        if (deste.Count >= 8)//Destede yeterince kart varsa kartları dağıt
        {
            int sonKart = deste.Count - 1;
            for (int i = sonKart; i > sonKart - 4; i--)
            {
                p1.kart.Add(deste[i]);
                p2.kart.Add(deste[i - 4]);
            }
            for (int i = sonKart; i > sonKart - 8; i--)
            {
                deste.RemoveAt(i);
            }
            gmUIManager.DesteDagit();
            el++;
        }
        else//yoksa oyunu bitir.Puanları hesaplat
        {
            el = 0;
            Debug.Log("Hamur bitti :D Destede kart kalmadı!!");
            PuanHesapla();
            if (yerdekiKartlar.Count > 0)//Destede kart kalmadıgında yerde ki kartları en son kazanana ver.
            {
                for (int i = 0; i < yerdekiKartlar.Count; i++)
                {
                    sonKazanan.kazanilanKartlar.Add(yerdekiKartlar[i]);
                }

                if (sonKazanan == p1)
                    gmUIManager.KazanilanKart(yerdekiKartlar[yerdekiKartlar.Count - 1].Split('_')[1], false);
                else if (sonKazanan == p2)
                    gmUIManager.KazanilanKart(yerdekiKartlar[yerdekiKartlar.Count - 1].Split('_')[1], true);
                yerdekiKartlar.Clear();

            }
            //puanları hesapla

            OynuBaSlat();

        }
    }

    private void PuanHesapla()
    {
        p1.puan = 0;
        p2.puan = 0;
        #region PistiPuanları
        if (p1.pistiler.Count > 0)
        {
            foreach (string item in p1.pistiler)
            {
                if (item.Split('_')[1] == "11")
                    p1.puan += 20;
                else
                {
                    p1.puan += 10;
                }
            }
        }
        if (p2.pistiler.Count > 0)
        {
            foreach (string item in p2.pistiler)
            {
                if (item.Split('_')[1] == "11")
                    p2.puan += 20;
                else
                {
                    p2.puan += 10;
                }
            }
        }
        #endregion

        
        #region DigerPuanlar
        if (p1.kazanilanKartlar.Count > 0)
        {
            foreach (string item in p1.kazanilanKartlar)
            {
                if (item == "C_2")
                    p1.puan += 2;
                else if (item == "D_10")
                    p1.puan += 3;
                else if (item.Split('_')[1] == "11")
                    p1.puan += 1;
                else if (item.Split('_')[1] == "1")
                    p1.puan += 1;


            }
            if (sonKazanan == p1 && deste.Count == 0)
                p1.puan += 3;
        }
        if (p2.kazanilanKartlar.Count > 0)
        {
            foreach (string item in p2.kazanilanKartlar)
            {
                if (item == "C_2")
                    p2.puan += 2;
                else if (item == "D_10")
                    p2.puan += 3;
                else if (item.Split('_')[1] == "11")
                    p2.puan += 3;
                else if (item.Split('_')[1] == "1")
                    p2.puan += 1;


            }
            if (sonKazanan == p2 && deste.Count == 0)
                p2.puan += 3;

        }

        #endregion
        if (deste.Count == 0)
        {
            p1.genelPuan += p1.puan;
            p2.genelPuan += p2.puan;
            gmUIManager.SkorYazdir(p1.genelPuan, false);
            gmUIManager.SkorYazdir(p2.genelPuan, true);
            p1.puan = 0;
            p2.puan = 0;
        }

        



    }

    private void OynuKur()
    {
        deste.Clear();
        yerdekiKartlar.Clear();
        yerdekiKartlar.Clear();

        p1.kazanilanKartlar.Clear();
        p1.kart.Clear();
        p1.pistiler.Clear();
        p2.kazanilanKartlar.Clear();
        p2.kart.Clear();
        p2.pistiler.Clear();


        DestayiOlustur();
        DesteyiKaristir();
        DesteyiDagit();
        gmUIManager.OyunKur();
        tur++;
        el++;
        if (tur > 7 || p1.genelPuan > 130 || p2.genelPuan > 130)
            GameObject.FindObjectOfType<CanvasManager>().Kazandin();
    }

    private void DesteyiDagit()
    {
        for (int i = 0; i < 4; i++)
        {
            yerdekiFazlaKartlar.Add(deste[i]);
            p1.kart.Add(deste[i + 4]);
            p2.kart.Add(deste[i + 8]);
        }
        for (int i = 0; i < 12; i++)//desteden alınan kartları çıkart
        {
            deste.RemoveAt(i);
        }

        gmUIManager.DesteDagit();
    }

    private void DesteyiKaristir()
    {
        int n = deste.Count;


        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);
            string value = deste[k];
            deste[k] = deste[n];
            deste[n] = value;


        }
    }

    private void DestayiOlustur()
    {
        /*
         * 1 As
         * 11 J,12 Q ,13 K 
         */
        for (int i = 1; i < 14; i++)
        {
            deste.Add("D_" + i);
            deste.Add("H_" + i);
            deste.Add("S_" + i);
            deste.Add("C_" + i);
        }
    }

    protected virtual void PistiOldu(EventArgs e)
    {
        EventHandler handler = Pisti;

        if (handler != null)
        {
            handler(this, e);
        }
    }

    protected virtual void YerdenKartAl(EventArgs e)
    {
        EventHandler handler = YerdenKartAlindi;
        if (handler != null)
        {
            handler(this, e);
        }
    }

    protected virtual void Hamle(EventArgs e)
    {
        EventHandler handler = HamleYapma;
        if (handler != null)
        {
            handler(this, e);
        }
    }

    protected virtual void YerdekiFazlaKartlaralindi(EventArgs e)
    {

        EventHandler handler = YerdekiFazlaKartlarAlindi;
        if (handler != null)
        {
            handler(this, e);
        }
    }
    public event EventHandler Pisti;
    public event EventHandler YerdenKartAlindi;
    public event EventHandler HamleYapma;
    public event EventHandler YerdekiFazlaKartlarAlindi;
}
[System.Serializable]
public class Player
{
    public Player(string ism)
    {
        isim = ism;
    }
    public string isim;//oyuncunun adı
    public int puan = 0;
    public int genelPuan = 0;
    public List<string> kazanilanKartlar = new List<string>();//oyuncunun yerden aldigi kartlar.
    public List<string> kart = new List<string>(4);//her oyuncu her seferinde yanlızca 4 kart ala bilir.
    public List<string> pistiler = new List<string>();//oyuncunun yabdigi pistiler.

    public string Oyna(string sonKart, int yerdekiKartSayisi)
    {

        if (yerdekiKartSayisi > 0)//eger yerde zaten kart varsa
        {
            bool sonkartVarmi = false;
            int sonkartIndex = 0;
            for (int i = 0; i < kart.Count; i++)
            {
                if (kart[i].Split('_')[1] == sonKart.Split('_')[1])
                {
                    sonkartVarmi = true;
                    sonkartIndex = i;
                }
            }
            if (sonkartVarmi)//son kart elimde var mı?
            {
                return kart[sonkartIndex];//varsa yere at
            }
            else
            {
                return kart[UnityEngine.Random.Range(0, kart.Count)];//yoksa rastgele bir kart at
            }
        }
        else
        {
            return kart[UnityEngine.Random.Range(0, kart.Count)];//yerde hiç kart yoksa rastgele bir kart at
        }




    }
}